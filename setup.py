from setuptools import setup

setup(
    name="custom_password_generator",
    version="20220701.0",
    packages=["custom_password_generator"],
    url="https://gitlab.com/daxm/python-password-generator",
    license="BSD",
    author="Dax Mickelson",
    author_email="dmickelson@zscaler.com",
    description="Generate a customized complex password",
)
