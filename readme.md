# Python Password Generator
This is just a simple way to generate a "random" string of characters.  I've used it mostly for generating passwords.

What I wanted was the ability to control how many of each "category" of characters (upper/lower case, numbers, punctuation) 
as well as the ability to filter out certain characters.  (Some places limit what is usable in their passwords.)

# How to get it
It is in [pypi](https://pypi.org/project/custom-password-generator/).

# How To Use
View the [example.py](example.py) for an example.

## ToDos
I think it would be nice to expand to extended ASCII characters.  Not sure how to add this right now but just a thought.
