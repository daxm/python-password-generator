from custom_password_generator import password_generator

upper_count = 2
lower_count = 3
numeric_count = 4
punctuation_count = 5
excluding = "4Rz@"

password = password_generator(
    uppercase_count=upper_count,
    lowercase_count=lower_count,
    numeric_count=numeric_count,
    punctuation_count=punctuation_count,
    exclude=excluding,
)

print(password)
